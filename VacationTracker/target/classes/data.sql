INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','ADMIN');
              
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(1,20,2019,3);
			
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(2,24,2020,3);
			
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(3,22,2021,3);
			
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(4,20,2019,2);
			
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(5,24,2020,2);
			
INSERT INTO total_vacation_days_per_year(id,number_of_days,year,korisnik_id)
			VALUES(6,22,2021,2);
			
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(1,'2019-12-01',3,'2019-12-03',2019,3);
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(2,'2020-12-01',3,'2020-12-03',2020,3);
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(3,'2021-12-01',3,'2021-12-03',2021,3);
			
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(4,'2019-12-01',3,'2019-12-03',2019,2);
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(5,'2020-12-01',3,'2020-12-03',2020,2);
			
INSERT INTO used_vacation_days(id,start_of_vacation,number_of_days_used,end_of_vacation,year,korisnik_id)
			VALUES(6,'2021-12-01',3,'2021-12-03',2021,2);
			

