package com.ftninformatika.jwd.vacationTracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class VacationTrackerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication.run(VacationTrackerApplication.class, args);

	}

}
