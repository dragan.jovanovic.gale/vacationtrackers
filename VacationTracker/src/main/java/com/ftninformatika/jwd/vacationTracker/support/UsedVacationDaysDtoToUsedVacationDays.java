package com.ftninformatika.jwd.vacationTracker.support;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.vacationTracker.dto.UsedVacationDaysDto;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.service.KorisnikService;

@Component
public class UsedVacationDaysDtoToUsedVacationDays implements Converter<UsedVacationDaysDto, UsedVacationDays> {

	@Autowired
	private KorisnikService korisnikService;

	@Override
	public UsedVacationDays convert(UsedVacationDaysDto dto) {
		UsedVacationDays entity;

		entity = new UsedVacationDays();

		if (entity != null) {
			int sumOfWeekendDays = 0;
			entity.setNumberOfDaysUsed(0);
			entity.setStartOfVacation(getLocalDate(dto.getStartDate()));
			entity.setEndOfVacation(getLocalDate(dto.getEndDate()));
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			entity.setKorisnik(korisnikService.findbyKorisnickoIme(username).get());
			Period period = Period.between(entity.getStartOfVacation(), entity.getEndOfVacation());
			long daysBetween = ChronoUnit.DAYS.between(entity.getStartOfVacation(), entity.getEndOfVacation());
			for (int i = 0; i <= daysBetween; i++) {

				LocalDate currentDate = entity.getStartOfVacation().plusDays(i);
				if (currentDate.getDayOfWeek() == DayOfWeek.SATURDAY
						|| currentDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
					sumOfWeekendDays++;
				}
			}
			entity.setNumberOfDaysUsed(period.getDays() + 1 - sumOfWeekendDays);
			entity.setYear(entity.getEndOfVacation().getYear());
			sumOfWeekendDays = 0;
		}

		return entity;
	}

	private LocalDate getLocalDate(String dateTime) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMMM d, yyyy");
		return LocalDate.parse(dateTime, formatter);
	}

}
