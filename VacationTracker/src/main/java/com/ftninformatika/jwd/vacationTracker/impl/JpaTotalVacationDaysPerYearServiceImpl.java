package com.ftninformatika.jwd.vacationTracker.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.model.TotalVacationDaysPerYear;
import com.ftninformatika.jwd.vacationTracker.repository.TotalVacationDaysPerYearRepository;
import com.ftninformatika.jwd.vacationTracker.service.KorisnikService;
import com.ftninformatika.jwd.vacationTracker.service.TotalVacationDaysPerYearService;

@Service
public class JpaTotalVacationDaysPerYearServiceImpl implements TotalVacationDaysPerYearService {

	@Autowired
	private TotalVacationDaysPerYearRepository repository;

	@Autowired
	private KorisnikService korisnikService;

	@Override
	public TotalVacationDaysPerYear delete(Long id) {
		Optional<TotalVacationDaysPerYear> totalVacationDaysPerYear = Optional.of(findOneById(id));
		if (totalVacationDaysPerYear.isPresent()) {
			repository.deleteById(id);
			return totalVacationDaysPerYear.get();
		}
		return null;
	}

	@Override
	public Page<TotalVacationDaysPerYear> find(Long korisnikId, Integer pageNo) {
		return repository.findByKorisnikId(korisnikId, PageRequest.of(pageNo, 2));
	}

	@Override
	public TotalVacationDaysPerYear save(TotalVacationDaysPerYear totalVacationDaysPerYear) {
		return repository.save(totalVacationDaysPerYear);
	}

	@Override
	public TotalVacationDaysPerYear update(TotalVacationDaysPerYear totalVacationDaysPerYear) {
		// TODO Auto-generated method stub
		return repository.save(totalVacationDaysPerYear);
	}

	@Override
	public TotalVacationDaysPerYear findOneById(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<TotalVacationDaysPerYear> loadVacationDays(InputStream is) {

		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"))) {
			String firstLine = fileReader.readLine();
			int year = Integer.parseInt(firstLine.substring(firstLine.length() - 4, firstLine.length()));
			CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
					.withHeader("Employee", "Total vacation days").withIgnoreHeaderCase().withTrim());
			List<TotalVacationDaysPerYear> vacationDays = new ArrayList<TotalVacationDaysPerYear>();
			Iterable<CSVRecord> csvRecords = csvParser.getRecords();

			for (CSVRecord csvRecord : csvRecords) {
				TotalVacationDaysPerYear vacationDay = new TotalVacationDaysPerYear();
				vacationDay.setNumberOfDays(Integer.parseInt(csvRecord.get("Total vacation days")));
				Optional<Korisnik> korisnikOptional = korisnikService.findbyKorisnickoIme(
						csvRecord.get("Employee").substring(0, csvRecord.get("Employee").length() - 7));
				if (korisnikOptional.isPresent()) {
					vacationDay.setKorisnik(korisnikOptional.get());
				}
				vacationDay.setYear(year);
				vacationDays.add(vacationDay);
			}
			return vacationDays;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}
	}

	public static String TYPE = "text/csv";
	static String[] HEADERs = { "Employee", "Total vacation days" };

	public static boolean hasCSVFormat(MultipartFile file) {

		if (!TYPE.equals(file.getContentType())) {
			return false;
		}

		return true;
	}

	@Override
	public TotalVacationDaysPerYear findByKorisnikKorisnickoImeAndYear(String userName, int year) {
		return repository.findByKorisnikKorisnickoImeAndYear(userName, year);
	}

}
