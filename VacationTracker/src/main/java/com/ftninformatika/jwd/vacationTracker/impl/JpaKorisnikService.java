package com.ftninformatika.jwd.vacationTracker.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.dto.KorisnikPromenaLozinkeDto;
import com.ftninformatika.jwd.vacationTracker.enumer.KorisnickaUloga;
import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.repository.KorisnikRepository;
import com.ftninformatika.jwd.vacationTracker.service.KorisnikService;

@Service
public class JpaKorisnikService implements KorisnikService {

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Optional<Korisnik> findOne(Long id) {
		return korisnikRepository.findById(id);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}

	@Override
	public Page<Korisnik> findAll(int brojStranice) {
		return korisnikRepository.findAll(PageRequest.of(brojStranice, 10));
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnik.setUloga(KorisnickaUloga.KORISNIK);
		return korisnikRepository.save(korisnik);
	}

	@Override
	public void delete(Long id) {
		korisnikRepository.deleteById(id);
	}

	@Override
	public Optional<Korisnik> findbyKorisnickoIme(String korisnickoIme) {
		return korisnikRepository.findFirstByKorisnickoIme(korisnickoIme);
	}

	@Override
	public boolean changePassword(Long id, KorisnikPromenaLozinkeDto korisnikPromenaLozinkeDto) {
		Optional<Korisnik> rezultat = korisnikRepository.findById(id);
		
		if (!rezultat.isPresent()) {
			throw new EntityNotFoundException();
		}
		Korisnik korisnik = rezultat.get();

		if (!korisnik.getKorisnickoIme().equals(korisnikPromenaLozinkeDto.getKorisnickoIme())
				|| !korisnik.getLozinka().equals(korisnikPromenaLozinkeDto.getLozinka())) {
			return false;
		}
		String password = korisnikPromenaLozinkeDto.getLozinka();
		if (!korisnikPromenaLozinkeDto.getLozinka().equals("")) {
			password = passwordEncoder.encode(korisnikPromenaLozinkeDto.getLozinka());
		}
		korisnik.setLozinka(password);
		korisnikRepository.save(korisnik);
		return true;
	}

	@Override
	public List<Korisnik> loadUsers(InputStream is) {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"))) {
			String firstLine = fileReader.readLine();
			CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
					.withHeader("Employee Email", "Employee Password").withIgnoreHeaderCase().withTrim());
			List<Korisnik> users = new ArrayList<Korisnik>();

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();

			for (CSVRecord csvRecord : csvRecords) {
				Korisnik korisnik = new Korisnik();
				korisnik.seteMail(csvRecord.get("Employee Email"));
				korisnik.setKorisnickoIme(
						csvRecord.get("Employee Email").substring(0, csvRecord.get("Employee Email").length() - 7));
				korisnik.setLozinka(passwordEncoder.encode(csvRecord.get("Employee Password")));
				korisnik.setUloga(KorisnickaUloga.KORISNIK);
				users.add(korisnik);
			}
			return users;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}
	}

	public static String TYPE = "text/csv";
	static String[] HEADERs = { "Employee Email", "Employee Password" };

	public static boolean hasCSVFormat(MultipartFile file) {
		if (!TYPE.equals(file.getContentType())) {
			return false;
		}
		return true;
	}

}
