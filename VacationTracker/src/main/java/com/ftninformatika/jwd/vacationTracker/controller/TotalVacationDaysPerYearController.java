package com.ftninformatika.jwd.vacationTracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.impl.JpaTotalVacationDaysPerYearServiceImpl;
import com.ftninformatika.jwd.vacationTracker.service.CsvFileService;
import com.ftninformatika.jwd.vacationTracker.support.ResponseMessage;

@Controller
@RequestMapping("/api/vacation")
public class TotalVacationDaysPerYearController {

	@Autowired
	private CsvFileService fileService;

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/upload")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
		String message = "";

		if (JpaTotalVacationDaysPerYearServiceImpl.hasCSVFormat(file)) {
			try {
				fileService.saveVacationDays(file);

				message = "Uploaded the file successfully: " + file.getOriginalFilename();
				return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
			} catch (Exception e) {
				message = "Could not upload the file: " + file.getOriginalFilename() + "!";
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
			}
		}

		message = "Please upload a csv file!";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	}

}
