package com.ftninformatika.jwd.vacationTracker.service;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;

public interface UsedVacationDaysService {

	UsedVacationDays delete(Long id);

	Page<UsedVacationDays> findByDatesAndKorisnikKorisnickoIme(String userName, LocalDate start, LocalDate end, Integer pageNo);

	UsedVacationDays findOneById(Long id);

	UsedVacationDays save(UsedVacationDays usedVacationDays);

	UsedVacationDays update(UsedVacationDays usedVacationDays);

	List<UsedVacationDays> loadUsedDays(InputStream is);

	List<UsedVacationDays> findByKorisnikKorisnickoImeAndYear(String userName, int year);

}
