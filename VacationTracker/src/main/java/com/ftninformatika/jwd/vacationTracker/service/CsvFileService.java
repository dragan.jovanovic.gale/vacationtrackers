package com.ftninformatika.jwd.vacationTracker.service;

import org.springframework.web.multipart.MultipartFile;

public interface CsvFileService {

	void saveVacationDays(MultipartFile file);

	void saveUsers(MultipartFile file);

	void saveUsedVacationDays(MultipartFile file);

}
