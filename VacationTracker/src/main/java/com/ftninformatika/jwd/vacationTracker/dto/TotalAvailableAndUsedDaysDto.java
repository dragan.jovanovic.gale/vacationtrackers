package com.ftninformatika.jwd.vacationTracker.dto;

public class TotalAvailableAndUsedDaysDto {

	private int availableDays;

	private int usedDays;

	private int totalDays;

	public int getAvailableDays() {
		return availableDays;
	}

	public void setAvailableDays(int availableDays) {
		this.availableDays = availableDays;
	}

	public int getUsedDays() {
		return usedDays;
	}

	public void setUsedDays(int usedDays) {
		this.usedDays = usedDays;
	}

	public int getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(int totalDays) {
		this.totalDays = totalDays;
	}

}
