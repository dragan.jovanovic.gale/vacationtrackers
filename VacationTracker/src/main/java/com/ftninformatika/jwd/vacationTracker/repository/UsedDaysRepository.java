package com.ftninformatika.jwd.vacationTracker.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;

public interface UsedDaysRepository extends JpaRepository<UsedVacationDays, Long> {

	
	Page<UsedVacationDays> findByKorisnikKorisnickoImeAndStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual
	(String userName,LocalDate startOfVacation,LocalDate endOfVacation, Pageable p);

	Page<UsedVacationDays> findByStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual
	(LocalDate startOfVacation,LocalDate endOfVacation, Pageable p);
	
	UsedVacationDays findOneById(Long id);
	
	List<UsedVacationDays> findByKorisnikKorisnickoImeAndYear(String userName,int year);

}
