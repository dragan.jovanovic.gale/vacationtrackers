package com.ftninformatika.jwd.vacationTracker.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.repository.UsedDaysRepository;
import com.ftninformatika.jwd.vacationTracker.service.KorisnikService;
import com.ftninformatika.jwd.vacationTracker.service.UsedVacationDaysService;

@Service
public class JpaUsedVacationDaysImpl implements UsedVacationDaysService {

	@Autowired
	private UsedDaysRepository usedDaysRepository;

	@Autowired
	private KorisnikService korisnikService;

	@Override
	public UsedVacationDays delete(Long id) {
		Optional<UsedVacationDays> totalVacationDaysPerYear = Optional.of(findOneById(id));
		if (totalVacationDaysPerYear.isPresent()) {
			usedDaysRepository.deleteById(id);
			return totalVacationDaysPerYear.get();
		}
		return null;
	}

	@Override
	public UsedVacationDays findOneById(Long id) {
		return usedDaysRepository.findOneById(id);
	}

	@Override
	public UsedVacationDays save(UsedVacationDays usedVacationDays) {
		return usedDaysRepository.save(usedVacationDays);
	}

	@Override
	public UsedVacationDays update(UsedVacationDays usedVacationDays) {
		return usedDaysRepository.save(usedVacationDays);
	}

	@Override
	public List<UsedVacationDays> loadUsedDays(InputStream is) {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				CSVParser csvParser = new CSVParser(fileReader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
			List<UsedVacationDays> usedVacationDaysList = new ArrayList<UsedVacationDays>();

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();

			korisnikService.findbyKorisnickoIme(TYPE);
			int sumOfWeekendDays=0;

			for (CSVRecord csvRecord : csvRecords) {
				UsedVacationDays usedVacationDays = new UsedVacationDays();
				
				Optional<Korisnik> korisnikOptional = korisnikService.findbyKorisnickoIme(
						csvRecord.get("Employee").substring(0, csvRecord.get("Employee").length() - 7));
				if (korisnikOptional.isPresent()) {
					usedVacationDays.setKorisnik(korisnikOptional.get());
				}

				usedVacationDays.setYear(Integer.parseInt(csvRecord.get("Vacation end date").substring(
						csvRecord.get("Vacation end date").length() - 4, csvRecord.get("Vacation end date").length())));

				usedVacationDays.setStartOfVacation(getLocalDate(csvRecord.get("Vacation start date")));
				usedVacationDays.setEndOfVacation(getLocalDate(csvRecord.get("Vacation end date")));
				Period period = Period.between(usedVacationDays.getStartOfVacation(),
						usedVacationDays.getEndOfVacation());
				long daysBetween = ChronoUnit.DAYS.between(usedVacationDays.getStartOfVacation(), usedVacationDays.getEndOfVacation());
				for (int i = 0; i <= daysBetween; i++) {
					
				    LocalDate currentDate = usedVacationDays.getStartOfVacation().plusDays(i);
				    if (currentDate.getDayOfWeek() == DayOfWeek.SATURDAY || currentDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
				    	sumOfWeekendDays++;
				    }
				}
				
				usedVacationDays.setNumberOfDaysUsed(period.getDays() + 1-sumOfWeekendDays);
				usedVacationDaysList.add(usedVacationDays);
				sumOfWeekendDays=0;
			}

			return usedVacationDaysList;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}

	}

	public static String TYPE = "text/csv";
	static String[] HEADERs = { "Employee", "Vacation start date", "Vacation end date" };

	public static boolean hasCSVFormat(MultipartFile file) {

		if (!TYPE.equals(file.getContentType())) {
			return false;
		}

		return true;
	}

	private LocalDate getLocalDate(String dateTime) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMMM d, yyyy");
		return LocalDate.parse(dateTime, formatter);
	}

	@Override
	public Page<UsedVacationDays> findByDatesAndKorisnikKorisnickoIme(String userName, LocalDate start, LocalDate end,
			Integer pageNo) {
		return usedDaysRepository.findByKorisnikKorisnickoImeAndStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual(
				userName, start, end, PageRequest.of(pageNo, 2));
	}


	@Override
	public List<UsedVacationDays> findByKorisnikKorisnickoImeAndYear(String userName, int year) {
		return usedDaysRepository.findByKorisnikKorisnickoImeAndYear(userName, year);
	}

}
