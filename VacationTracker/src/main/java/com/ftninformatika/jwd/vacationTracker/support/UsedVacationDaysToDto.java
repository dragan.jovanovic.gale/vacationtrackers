package com.ftninformatika.jwd.vacationTracker.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.vacationTracker.dto.UsedVacationDaysDto;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;

@Component
public class UsedVacationDaysToDto implements Converter<UsedVacationDays, UsedVacationDaysDto> {

	@Override
	public UsedVacationDaysDto convert(UsedVacationDays source) {
		UsedVacationDaysDto dto = new UsedVacationDaysDto();
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		dto.setUsername(username);
		dto.setStartDate(source.getStartOfVacation().toString());
		dto.setEndDate(source.getEndOfVacation().toString());
		return dto;
	}

	public List<UsedVacationDaysDto> convert(List<UsedVacationDays> list) {
		List<UsedVacationDaysDto> dto = new ArrayList<>();
		for (UsedVacationDays usedVacationDays : list) {
			dto.add(convert(usedVacationDays));
		}
		return dto;

	}

}
