package com.ftninformatika.jwd.vacationTracker;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.model.TotalVacationDaysPerYear;
import com.ftninformatika.jwd.vacationTracker.repository.KorisnikRepository;
import com.ftninformatika.jwd.vacationTracker.repository.TotalVacationDaysPerYearRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TotalVacationDaysPerYearRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private TotalVacationDaysPerYearRepository repository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Test
	public void testFindByKorisnikId() {
		// given

		TotalVacationDaysPerYear tv1 = new TotalVacationDaysPerYear();

		tv1.setId(1L);
		tv1.setNumberOfDays(15);
		tv1.setYear(2020);
		Korisnik user1 = new Korisnik();
		user1 = korisnikRepository.findById(1L).get();
		tv1.setKorisnik(user1);

		TotalVacationDaysPerYear tv2 = new TotalVacationDaysPerYear();

		tv2.setNumberOfDays(20);
		tv2.setYear(2021);
		Korisnik user2 = new Korisnik();
		user2 = korisnikRepository.findById(2L).get();
		tv2.setKorisnik(user2);

		TotalVacationDaysPerYear tv3 = new TotalVacationDaysPerYear();

		tv3.setNumberOfDays(10);
		tv3.setYear(2020);
		Korisnik user3 = new Korisnik();
		user3 = korisnikRepository.findById(3L).get();
		tv3.setKorisnik(user3);

		entityManager.merge(tv1);
		entityManager.merge(tv2);
		entityManager.merge(tv3);
		entityManager.flush();

		Page<TotalVacationDaysPerYear> result = repository.findByKorisnikId(1L, PageRequest.of(0, 10));
		System.out.println(result);
		System.out.println(tv1);
		assertThat(result.getTotalElements()).isEqualTo(1);
	}

	@Test
	public void testFindOneById() {

		TotalVacationDaysPerYear tv1 = new TotalVacationDaysPerYear();
		tv1.setNumberOfDays(15);
		tv1.setYear(2020);
		Korisnik user1 = new Korisnik();
		user1 = korisnikRepository.findById(2L).get();

		entityManager.persist(tv1);
		entityManager.flush();

	
		TotalVacationDaysPerYear result = repository.findOneById(tv1.getId());

		
		assertThat(result).isEqualTo(tv1);
	}

    @Test
    public void testFindByKorisnikKorisnickoImeAndYear() {
        // given
        TotalVacationDaysPerYear tv1 = new TotalVacationDaysPerYear();
        tv1.setId(1L);
        tv1.setNumberOfDays(15);
        tv1.setYear(2020);
        Korisnik user1=new Korisnik();
        user1=korisnikRepository.findById(1L).get();
        tv1.setKorisnik(user1);
        TotalVacationDaysPerYear tv2 = new TotalVacationDaysPerYear();
        entityManager.merge(tv1);
        entityManager.merge(tv2);
        entityManager.flush();

    
        TotalVacationDaysPerYear result = repository.findByKorisnikKorisnickoImeAndYear("miroslav", 2020);

        assertThat(result).isEqualTo(tv1);
    }


}
