package com.ftninformatika.jwd.vacationTracker;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.ftninformatika.jwd.vacationTracker.enumer.KorisnickaUloga;
import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.repository.KorisnikRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class KorisnikRepositoryTest {
	

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private KorisnikRepository repository;


	
	@Test
	public void testFindFirstByKorisnickoIme() {
	    Korisnik user1 = new Korisnik();
	    user1.setKorisnickoIme("firstuser");
	    user1.seteMail("someemail@gmail.com");
	    user1.setLozinka("somepassword");
	    user1.setUloga(KorisnickaUloga.KORISNIK);
	    entityManager.persist(user1);
	    entityManager.flush();


	    Optional<Korisnik> result = repository.findFirstByKorisnickoIme("firstuser");

	    assertThat(result).isPresent();
	    assertThat(result.get()).isEqualTo(user1);
	}
}
