package com.ftninformatika.jwd.vacationTracker;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.repository.KorisnikRepository;
import com.ftninformatika.jwd.vacationTracker.repository.UsedDaysRepository;

import antlr.collections.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UsedVacationDaysRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UsedDaysRepository repository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Test
	public void testFindByKorisnikKorisnickoImeAndStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual() {
		// given
		UsedVacationDays uvd1 = new UsedVacationDays();
		uvd1.setId(1L);
		uvd1.setStartOfVacation(LocalDate.of(2020, 1, 1));
		uvd1.setEndOfVacation(LocalDate.of(2020, 1, 5));
		Korisnik user1 = new Korisnik();
		user1 = korisnikRepository.findById(1L).get();
		uvd1.setKorisnik(user1);

		UsedVacationDays uvd2 = new UsedVacationDays();
		uvd2.setStartOfVacation(LocalDate.of(2020, 2, 1));
		uvd2.setEndOfVacation(LocalDate.of(2020, 2, 5));
		Korisnik user2 = new Korisnik();
		user2 = korisnikRepository.findById(2L).get();
		uvd2.setKorisnik(user2);

		UsedVacationDays uvd3 = new UsedVacationDays();
		uvd3.setStartOfVacation(LocalDate.of(2020, 3, 1));
		uvd3.setEndOfVacation(LocalDate.of(2020, 3, 5));
		Korisnik user3 = new Korisnik();
		user3 = korisnikRepository.findById(3L).get();
		uvd3.setKorisnik(user3);

		entityManager.merge(uvd1);
		entityManager.merge(uvd2);
		entityManager.merge(uvd3);
		entityManager.flush();

		Page<UsedVacationDays> result = repository
				.findByKorisnikKorisnickoImeAndStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual("miroslav",
						LocalDate.of(2020, 1, 1), LocalDate.of(2020, 3, 5), PageRequest.of(0, 10));
		assertThat(result.getTotalElements()).isEqualTo(1);
	}

	@Test
	public void testFindByStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual() {

		UsedVacationDays uvd1 = new UsedVacationDays();
		uvd1.setStartOfVacation(LocalDate.of(2020, 1, 1));
		uvd1.setEndOfVacation(LocalDate.of(2020, 1, 5));
		Korisnik user1 = new Korisnik();
		user1 = korisnikRepository.findById(1L).get();
		uvd1.setKorisnik(user1);

		UsedVacationDays uvd2 = new UsedVacationDays();
		uvd2.setStartOfVacation(LocalDate.of(2020, 2, 1));
		uvd2.setEndOfVacation(LocalDate.of(2020, 2, 5));
		Korisnik user2 = new Korisnik();
		user2 = korisnikRepository.findById(2L).get();
		uvd2.setKorisnik(user2);

		UsedVacationDays uvd3 = new UsedVacationDays();
		uvd3.setStartOfVacation(LocalDate.of(2020, 3, 1));
		uvd3.setEndOfVacation(LocalDate.of(2020, 4, 5));
		Korisnik user3 = new Korisnik();
		user3 = korisnikRepository.findById(3L).get();
		uvd3.setKorisnik(user3);

		entityManager.merge(uvd1);
		entityManager.merge(uvd2);
		entityManager.merge(uvd3);
		entityManager.flush();

		Page<UsedVacationDays> result = repository.findByStartOfVacationGreaterThanEqualAndEndOfVacationLessThanEqual(
				LocalDate.of(2020, 1, 1), LocalDate.of(2020, 3, 5), PageRequest.of(0, 10));
		assertThat(result.getTotalElements()).isEqualTo(2);
	}

	@Test
	public void testFindOneById() {
		UsedVacationDays uvd = new UsedVacationDays();
		uvd.setStartOfVacation(LocalDate.of(2020, 1, 1));
		uvd.setEndOfVacation(LocalDate.of(2020, 1, 5));
		Korisnik user = new Korisnik();
		user = korisnikRepository.findById(1L).get();
		uvd.setKorisnik(user);

		entityManager.persist(uvd);
		entityManager.flush();

		UsedVacationDays result = repository.findOneById(uvd.getId());

		assertThat(result).isEqualTo(uvd);
	}

	@Test
	public void testFindByKorisnikKorisnickoImeAndYear() {

		UsedVacationDays uvd1 = new UsedVacationDays();
		uvd1.setStartOfVacation(LocalDate.of(2020, 1, 1));
		uvd1.setEndOfVacation(LocalDate.of(2020, 1, 5));
		Korisnik user1 = new Korisnik();
		user1=korisnikRepository.findById(1L).get();
		uvd1.setKorisnik(user1);
		uvd1.setYear(uvd1.getEndOfVacation().getYear());

		UsedVacationDays uvd2 = new UsedVacationDays();
		uvd2.setStartOfVacation(LocalDate.of(2021, 1, 1));
		uvd2.setEndOfVacation(LocalDate.of(2021, 1, 5));
		Korisnik user2 = new Korisnik();
		user2=korisnikRepository.findById(2L).get();
		uvd2.setKorisnik(user2);
		uvd2.setYear(uvd2.getEndOfVacation().getYear());

		entityManager.persist(uvd1);
		entityManager.persist(uvd2);
		entityManager.flush();

		ArrayList<UsedVacationDays> result = (ArrayList<UsedVacationDays>) repository
				.findByKorisnikKorisnickoImeAndYear("miroslav", 2020);
		for(UsedVacationDays days:result) {
		System.out.println(days);
		}
		assertThat(result).containsExactly(uvd1);
	}

}
