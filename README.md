Set up a development environment:
Install the Java Development Kit (JDK)
Install an IDE, such as IntelliJ IDEA or Eclipse
Install a build tool, such as Maven or Gradle
Install a version control system, such as Git
Install a Rest Client such as Postman
Clone or download the project from the source repository using the version control system,https://gitlab.com/dragan.jovanovic.gale/vacationtrackers
Import the project through maven into your desired IDE
Start VacationTrackerApplication
Once the application is running,we can import desired sample files, that are provided in vacationTracker/src/main/resources
First off to be able to import users you need admin privileges, go to Postman ,create a new POST request to the following endpoint http://localhost:8080/api/korisnici/auth
Click on Body than Raw and send this credentials as JSON
As a note all imported user's username will be set to the first part of their email for example(user2@rbt.rs --> user2) 
 {
    "username": "miroslav",
    "password":"miroslav"
}
Once you successfully do that you will receive JSON Web Token, you need to copy that token and make a new POST request targeting following endpoint http://localhost:8080/api/korisnici/upload
Click on tab Authorization choose Bearer Token as type and paste the token you got or you can simply use this one:

eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaXJvc2xhdiIsInJvbGUiOnsiYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9LCJjcmVhdGVkIjoxNjU3NjI3NjAyMzU5LCJleHAiOjIyODg3Nzk2MDJ9.e-eDj_8JPIPuHZeG6MpcTF83udX4DDSRGjuF9DdnSHZOdo2PxQYTSOYh-NPKMoD6TpHGvSKC0ja7QaBI3vte4g

Go to body tab ,chose form-data, key name should be "file" and value should be csv file "employee_profiles".
If you check your database you should have rows of employees.
Do the previous steps(you can use same token) but target following endpoint to load Total vacation days per employee and year, http://localhost:8080/api/vacation/upload ,
you should use following csv files vacations_2019, vacations_2020,vacations_2021,
and for loading used vacation days target http://localhost:8080/api/usedVacation/upload, and chose used_vacation_dates.
Once that is done, you can log in as user(see previous instructions) using following credentials 
{
  "username": "petar"
  "password":"petar"
}
As user you can target new endpoints, http://localhost:8080/api/usedVacation as a POST request, resulting in a new vacation time, important point is that days during weekends will not count
as used vacation time, you need format of following JSON 
{
    "startDate":"Tuesday, September 10, 2019",
    "endDate":"Friday, September 13, 2019"
}

To get the used vacation time ,you need to target http://localhost:8080/api/usedVacation as a GET request, where as Params you will use following
startDate which  value will be in format 2000-01-01 , endDate which value will use same format,
and pageNo which takes in integer and it represents page number (If not entered it will be set to 0. Every page will have 2 items).


Finally there is another GET request, http://localhost:8080/api/AllDays, where params is integer year and as result you will get available free days for loged in user,
used days in that year, and total free days in that year.

Important notes, database is set up as CREATE-DROP meaning any changes made will revert once the application is stopped.

For any inquires you can contact me at, dragan.jovanovic.gale@gmail.com